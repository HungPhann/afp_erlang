-module(collatz).
-export([max_cycle/2]).

% new() -> Map
% put(Key, Value, Map1) -> Map2
% find(Key, Map) -> {ok, Value} | error


% find collatz cycle of N, provided Map to store previous values.
-spec collatz_cycle(pos_integer(), _) -> {pos_integer(), _}.
collatz_cycle(N, Map) ->
    Result = collatz_cycle_helper(N, 1, Map),
    NewMap = maps:put(N, Result, Map),
    {Result, NewMap}.
        
-spec collatz_cycle_helper(pos_integer(), pos_integer(), _) -> pos_integer().
collatz_cycle_helper(1, Result, _) -> Result;
collatz_cycle_helper(N, Result, Map) ->
    Find = maps:find(N, Map),
    case Find =:= error of 
        true ->
            case N rem 2 of 
                0 -> collatz_cycle_helper(N div 2, Result + 1, Map);
                1 -> collatz_cycle_helper(N * 3 + 1, Result + 1, Map)
            end;
        false ->
            {_, Value} = Find,
            Value + Result - 1
    end.

    
% find longest collatz cycle in [N, M]
-spec max_cycle(pos_integer(), pos_integer()) -> pos_integer().
max_cycle(N, M) -> 
    {Result, _} = max_cycle(N, M, 3, maps:new()),
    Result.

max_cycle(N, M, 0, Map) ->
    max_cycle_helper(N, M, N, 0, Map);

max_cycle(N, M, D, Map) ->
    C = (M + N) div 2,                                          % devide [N, M] into [N, C] and [C + 1, M]
    Par = self(),
    spawn_link(fun () -> 
                    Par ! max_cycle(N, C, D - 1, Map)           % calculate max_cycle(N, C) in other process.
                end),
    {Max_2, NewMap_2} = max_cycle(C + 1, M, D - 1, Map),        % calculate max_cycle(C + 1, M) in main process.
    {Max_1, NewMap_1} = receive Max -> Max end,
    case Max_1 > Max_2 of                                       % Calculate final result.
        true -> {Max_1, maps:merge(NewMap_1, NewMap_2)};
        false -> {Max_2, maps:merge(NewMap_1, NewMap_2)}
    end.
    

% main helper of max_cycle().
-spec max_cycle_helper(pos_integer(), pos_integer(), pos_integer(), integer(), _) -> {integer, _}.
max_cycle_helper(_, M, I, Result, Map) when I > M ->
    {Result, Map};
max_cycle_helper(N, M, I, Result, Map) ->
    {I_collatz_cycle, NewMap} = collatz_cycle(I, Map),
    case I_collatz_cycle > Result of
        true ->
            max_cycle_helper(N, M, I + 1, I_collatz_cycle, NewMap);
        false ->
            max_cycle_helper(N, M, I + 1, Result, NewMap)
    end.
    

