-module(collatz_backup).
-export([max_cycle/2]).

% new() -> Map
% put(Key, Value, Map1) -> Map2
% find(Key, Map) -> {ok, Value} | error

-spec collatz_cycle(pos_integer(), _) -> {pos_integer(), _}.
collatz_cycle(N, Map) ->
    Result = collatz_cycle_helper(N, 1, Map),
    NewMap = maps:put(N, Result, Map),
    {Result, NewMap}.
        


collatz_cycle_helper(1, Result, _) -> Result;
collatz_cycle_helper(N, Result, Map) ->
    Find = maps:find(N, Map),
    case Find =:= error of 
        true ->
            case N rem 2 of 
                0 -> collatz_cycle_helper(N div 2, Result + 1, Map);
                1 -> collatz_cycle_helper(N * 3 + 1, Result + 1, Map)
            end;
        false ->
            {_, Value} = Find,
            Value + Result - 1
    end.

    


-spec max_cycle(pos_integer(), pos_integer()) -> pos_integer().
max_cycle(N, M) -> max_cycle(N, M, N, 0, maps:new()).

max_cycle(_, M, I, Result, _) when I > M ->
    Result;
max_cycle(N, M, I, Result, Map) ->
    {I_collatz_cycle, NewMap} = collatz_cycle(I, Map),
    case I_collatz_cycle > Result of
        true ->
            max_cycle(N, M, I + 1, I_collatz_cycle, NewMap);
        false ->
            max_cycle(N, M, I + 1, Result, NewMap)
    end.
        