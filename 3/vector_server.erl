%%%-------------------------------------------------------------------
%%% @author Phan Huy Hung <Huyhung.Phan.0630@student.uu.se>
%%% @doc RPC over TCP server. This module defines a server process that
%%% calculate vector expression.
%%%
%%% @end
%%%-------------------------------------------------------------------

-module(vector_server).
-behaviour(gen_server).

%% API
-export([
    start_link/1,
    start_link/0,
    stop/0
    ]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,terminate/2, code_change/3]).

-define(SERVER, ?MODULE).
-define(DEFAULT_PORT, 1055).
-record(state, {port, lsock}).


%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc Starts the server.
%%
%% @spec start_link(Port::integer()) -> {ok, Pid}
%% where
%% Pid = pid()
%% @end
%%--------------------------------------------------------------------
start_link(Port) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [Port], []).

%% @spec start_link() -> {ok, Pid}
%% @doc Calls `start_link(Port)' using the default port.
start_link() ->
    start_link(?DEFAULT_PORT).


%%--------------------------------------------------------------------
%% @doc Stops the server.
%% @spec stop() -> ok
%% @end
%%--------------------------------------------------------------------
stop() ->
    gen_server:cast(?SERVER, stop).



%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%% --------------------------------------------------------------------
%% Function: init/1
%% Description: Initiates the server
%% Returns: {ok, State}          |
%%          {ok, State, Timeout} |
%%          ignore               |
%%          {stop, Reason}
%% --------------------------------------------------------------------
init([Port]) ->
    {ok, LSock} = gen_tcp:listen(Port, [{active, true}]),
    {ok, #state{port = Port, lsock = LSock}, 0}.


%% --------------------------------------------------------------------
%% Function: handle_cast/2
%% Description: Handling cast messages
%% Returns: {noreply, State}          |
%%          {noreply, State, Timeout} |
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------
handle_call(_, _From, State) ->
    {reply, {ok, ok, State}}.

handle_cast(stop, State) ->
    {stop, normal, State}.


%% --------------------------------------------------------------------
%% Function: handle_info/2
%% Description: Handling all non call/cast messages
%% Returns: {noreply, State}          |
%%          {noreply, State, Timeout} |
%%          {stop, Reason, State}            (terminate/2 is called)
%% --------------------------------------------------------------------
handle_info({tcp, Socket, RawData}, State) ->
    do_rpc(Socket, RawData),
    {noreply, State};

handle_info({tcp_closed, _Socket}, #state{lsock = LSock} = State) ->
    {ok, _Sock} = gen_tcp:accept(LSock),
    {noreply, State};
    % {stop, normal, State};

handle_info(timeout, #state{lsock = LSock} = State) ->
    {ok, _Sock} = gen_tcp:accept(LSock),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%%%===================================================================
%%% Internal functions
%%%===================================================================

do_rpc(Socket, RawData) ->
    try
        Input = re:replace(RawData, "\r\n$", "", [{return, list}]),
        Result = calculate_expr(convert(Input)),
        gen_tcp:send(Socket, io_lib:fwrite("Res: ~w~n", [Result]))
    catch
        _ ->
            Error = error,
            gen_tcp:send(Socket, io_lib:fwrite("Res: ~w~n", [Error]))
    end.


calculate_expr(Expr) ->
    case expr_depth(Expr) > 100 of
        true -> throw(expression_depth_out_of_bound_error);
        false ->
            try 
                calculate_expr_helper(Expr)
            catch 
                Error -> throw(Error)
            end
    end.

calculate_expr_helper({'add', Expr1, Expr2}) ->
    try
        expr_add(Expr1, Expr2)
    catch 
        E -> throw(E)
    end;

calculate_expr_helper({'sub', Expr1, Expr2}) ->
    try
        expr_subtract(Expr1, Expr2)
    catch 
        E -> throw(E)
    end;

calculate_expr_helper({'dot', Expr1, Expr2}) ->
    try
        expr_dot(Expr1, Expr2)
    catch 
        E -> throw(E)
    end;

calculate_expr_helper({'mul', Expr1, Expr2}) ->
    try
        expr_mul(Expr1, Expr2)
    catch 
        E -> throw(E)
    end;

calculate_expr_helper({'div', Expr1, Expr2}) ->
    try
        expr_div(Expr1, Expr2)
    catch 
        E -> throw(E)
    end;

calculate_expr_helper({'norm_one', Expr}) ->
    try
        expr_norm_one(Expr)
    catch 
        E -> throw(E)
    end;

calculate_expr_helper({'norm_inf', Expr}) ->
    try
        expr_norm_inf(Expr)
    catch 
        E -> throw(E)
    end;

calculate_expr_helper(Vector) ->
    Vector.



%%%===================================================================
%%% Add
%%%===================================================================
expr_add(Expr1, Expr2) ->
    try 
        case is_list(Expr1) andalso is_list(Expr2) of
            true -> 
                vector_add(Expr1, Expr2);
            false ->
                case is_list(Expr1) and is_tuple(Expr2) of 
                    true -> 
                        expr_add(Expr1, calculate_expr_helper(Expr2));
                    false ->
                        case is_tuple(Expr1) and is_list(Expr2) of 
                            true -> expr_add(calculate_expr_helper(Expr1), Expr2);
                            false -> expr_add(calculate_expr_helper(Expr1), calculate_expr_helper(Expr2))
                        end
                end
        end
    catch 
        E -> throw(E)
    end.

vector_add(Vector1, Vector2) ->
    case (length(Vector1) =:= length(Vector2)) and check_range_vector(Vector1) and check_range_vector(Vector2) of 
        true -> vector_add_helper(Vector1, Vector2);
        false -> throw(add_error)
    end.

vector_add_helper(Vector1, Vector2) -> vector_add_helper(Vector1, Vector2, []).
vector_add_helper([], [], Result) -> Result;
vector_add_helper([H1|T1], [H2|T2], Result) -> vector_add_helper(T1, T2, Result ++ [H1 + H2]).



%%%===================================================================
%%% Subtract
%%%===================================================================
expr_subtract(Expr1, Expr2) ->
    try 
        case is_list(Expr1) andalso is_list(Expr2) of
            true -> 
                vector_subtract(Expr1, Expr2);
            false ->
                case is_list(Expr1) and is_tuple(Expr2) of 
                    true -> 
                        expr_subtract(Expr1, calculate_expr_helper(Expr2));
                    false ->
                        case is_tuple(Expr1) and is_list(Expr2) of 
                            true -> expr_subtract(calculate_expr_helper(Expr1), Expr2);
                            false -> expr_subtract(calculate_expr_helper(Expr1), calculate_expr_helper(Expr2))
                        end
                end
        end
    catch 
        E -> throw(E)
    end.

vector_subtract(Vector1, Vector2) ->
    case (length(Vector1) =:= length(Vector2)) and check_range_vector(Vector1) and check_range_vector(Vector2) of 
        true -> vector_subtract_helper(Vector1, Vector2);
        false -> throw(subtract_error)
    end.

vector_subtract_helper(Vector1, Vector2) -> vector_subtract_helper(Vector1, Vector2, []).
vector_subtract_helper([], [], Result) -> Result;
vector_subtract_helper([H1|T1], [H2|T2], Result) -> vector_subtract_helper(T1, T2, Result ++ [H1 - H2]).



%%%===================================================================
%%% Dot
%%%===================================================================
expr_dot(Expr1, Expr2) ->
    try 
        case is_list(Expr1) andalso is_list(Expr2) of
            true -> 
                vector_dot(Expr1, Expr2);
            false ->
                case is_list(Expr1) and is_tuple(Expr2) of 
                    true -> 
                        expr_dot(Expr1, calculate_expr_helper(Expr2));
                    false ->
                        case  is_tuple(Expr1) and is_list(Expr2) of 
                            true -> expr_dot(calculate_expr_helper(Expr1), Expr2);
                            false -> expr_dot(calculate_expr_helper(Expr1), calculate_expr_helper(Expr2))
                        end
                end
        end
    catch 
        E -> throw(E)
    end.

vector_dot(Vector1, Vector2) ->
    case (length(Vector1) =:= length(Vector2)) and check_range_vector(Vector1) and check_range_vector(Vector2) of 
        true -> vector_dot_helper(Vector1, Vector2);
        false -> throw(dot_error)
    end.

vector_dot_helper(Vector1, Vector2) -> vector_dot_helper(Vector1, Vector2, []).
vector_dot_helper([], [], Result) -> Result;
vector_dot_helper([H1|T1], [H2|T2], Result) -> vector_dot_helper(T1, T2, Result ++ [H1 * H2]).



%%%===================================================================
%%% Mul
%%%===================================================================
expr_mul(Expr1, Expr2) ->
    try 
        case is_integer(Expr1) andalso is_list(Expr2) of
            true -> 
                vector_mul(Expr2, Expr1);
            false ->
                case is_integer(Expr1) and is_tuple(Expr2) of 
                    true -> 
                        expr_mul(Expr1, calculate_expr_helper(Expr2));
                    false ->
                        case is_tuple(Expr1) and is_list(Expr2) of 
                            true -> expr_mul(calculate_expr_helper(Expr1), Expr2);
                            false -> expr_mul(calculate_expr_helper(Expr1), calculate_expr_helper(Expr2))
                        end
                end
        end
    catch 
        E -> throw(E)
    end.

vector_mul(Vector, Number) ->
    case check_range_vector(Vector) of 
        true -> vector_mul_helper(Vector, Number);
        false -> throw(mul_error)
    end.

vector_mul_helper(Vector, Number) -> vector_mul_helper(Vector, Number, []).
vector_mul_helper([], _, Result) -> Result;
vector_mul_helper([H|T], Number, Result) -> vector_mul_helper(T, Number, Result ++ [H * Number]).



%%%===================================================================
%%% Div
%%%===================================================================
expr_div(Expr1, Expr2) ->
    try 
        case is_integer(Expr1) andalso is_list(Expr2) of
            true -> 
                vector_div(Expr2, Expr1);
            false ->
                case is_integer(Expr1) and is_tuple(Expr2) of 
                    true -> 
                        expr_div(Expr1, calculate_expr_helper(Expr2));
                    false ->
                        case is_tuple(Expr1) and is_list(Expr2) of 
                            true -> expr_div(calculate_expr_helper(Expr1), Expr2);
                            false -> expr_div(calculate_expr_helper(Expr1), calculate_expr_helper(Expr2))
                        end
                end
        end
    catch 
        E -> throw(E)
    end.

vector_div(Vector, Number) ->
    case check_range_vector(Vector) andalso Number /= 0 of 
        true -> vector_div_helper(Vector, Number);
        false -> throw(div_error)
    end.

vector_div_helper(Vector, Number) -> vector_div_helper(Vector, Number, []).
vector_div_helper([], _, Result) -> Result;
vector_div_helper([H|T], Number, Result) -> vector_div_helper(T, Number, Result ++ [H div Number]).



%%%===================================================================
%%% norm_one
%%%===================================================================
expr_norm_one(Expr) ->
    try
        case is_list(Expr) of 
            true -> vector_norm_one(Expr);
            false -> expr_norm_one(calculate_expr_helper(Expr))
        end
    catch 
        Error -> throw(Error)
    end.

vector_norm_one(Vector) -> 
    case check_range_vector(Vector) of 
        true -> vector_norm_one(Vector, 0);
        false -> throw(norm_one_error)
    end.

vector_norm_one([], Result) -> Result;
vector_norm_one([H|T], Result) -> vector_norm_one(T, Result + abs(H)).



%%%===================================================================
%%% norm_inf
%%%===================================================================
expr_norm_inf(Expr) ->
    try
        case is_list(Expr) of 
            true -> vector_norm_inf(Expr);
            false -> expr_norm_inf(calculate_expr_helper(Expr))
        end
    catch 
        Error -> throw(Error)
    end.

vector_norm_inf(Vector) -> 
    case check_range_vector(Vector) of 
        true -> vector_norm_inf(Vector, 0);
        false -> throw(norm_inf_error)
    end.

vector_norm_inf([], Result) -> Result;
vector_norm_inf([H|T], Result) -> 
    case abs(H) > Result of 
        true -> vector_norm_inf(T, abs(H));
        false -> vector_norm_inf(T, Result)
    end.



% convert raw input to Erlang expression
convert(S) -> 
    try 
        {ok, Ts, _} = erl_scan:string(S), 
        {ok, Result} = erl_parse:parse_term(Ts ++ [{dot,1} || element(1, lists:last(Ts)) =/= dot]), 
        Result
    catch 
        _ -> throw(cannot_parse_input_error)
    end.

% function for checking validation of input
check_range_vector(Vector) ->
    Length = length(Vector),
    case Length >= 1 andalso Length =< 100 of 
        true -> true;
        false -> false
    end.


% depth of expression
expr_depth({_, Expr}) ->            % norm
    1 + expr_depth(Expr);

expr_depth({_, Expr1, Expr2}) ->    % scalar_op, vector_op
    Depth1 = expr_depth(Expr1),
    Depth2 = expr_depth(Expr2),
    case Depth1 > Depth2 of 
        true -> 1 + Depth1;
        false -> 1 + Depth2
    end;

expr_depth(_) -> 0.                 % vector, number

