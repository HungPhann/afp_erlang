-module(test).
-export([random_string/1]).


random_string(N) ->
   random_string(N, []).

random_string(0, Acc) ->
   Acc;
random_string(N, Acc) ->
   random_string(N - 1, [rand:uniform(26) + 96 | Acc]).