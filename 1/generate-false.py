#!/usr/bin/env python3
import sys
import string
import random
n = int(sys.argv[1])

if n < 3:
    print('Undefined for n < 3')
    quit()

for i in range(1, n):
    print(i, end=' ')
print(n)

for i in range(n, 3, -1):
    print(i, end=' ')
print('2 3 1')

