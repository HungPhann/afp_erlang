-module(test).


main([Filename]) ->
    {ok, Device} = file:open(Filename, [read]),
    {_, Line1} = file:read_line(Device),
    {_, Line2} = file:read_line(Device),

    List2 = [list_to_integer(I) || I <- string:tokens(string:trim(Line1)," ")],    
    List1 = [list_to_integer(I) || I <- string:tokens(string:trim(Line2)," ")],
    io:fwrite(perm(List1, List2)).



% helper functions

% stack
-spec new_stack() -> queue:queue().
new_stack() ->
    queue:new().

-spec push_stack(queue:queue(T), T) -> queue:queue(T).
push_stack(Stack, Value) ->
    queue:in_r(Value, Stack).

-spec pop_stack(queue:queue(T)) -> {queue:queue(T), T}.
pop_stack(Stack) ->
    {{_, Value}, NewStack} = queue:out(Stack),
    {NewStack, Value}.

-spec peek_stack(queue:queue(T)) -> T.
peek_stack(Stack)->
    {_, Value} = queue:peek(Stack),
    Value.

-spec is_empty_stack(queue:queue()) -> boolean.
is_empty_stack(Stack) ->
    queue:is_empty(Stack).


% queue
-spec list_to_queue([T]) -> queue:queue(T).
list_to_queue(List) -> queue:from_list(List).

-spec dequeue(queue:queue(T)) -> {queue:queue(T), T}.
dequeue(Queue) ->
    {{_, Value}, NewQueue} = queue:out_r(Queue),
    {NewQueue, Value}.

-spec peek_queue(queue:queue(T)) -> T.
peek_queue(Queue) ->
    {_, Value} = queue:peek_r(Queue),
    Value.

-spec is_empty_queue(queue:queue()) -> boolean.
is_empty_queue(Queue) ->
    queue:is_empty(Queue).


% main logic

-spec perm([T], [T]) -> boolean.
perm(Input_List, Output_List) ->
    Input_Queue = list_to_queue(Input_List),
    Output_Queue = list_to_queue(Output_List),
    Stack = new_stack(),
    perm_helper_1(Input_Queue, Output_Queue, Stack).
    

-spec perm_helper_1(queue:queue(T), queue:queue(T), queue:queue(T)) -> boolean.
perm_helper_1(Input_Queue, Output_Queue, Stack) ->
    case is_empty_queue(Input_Queue) of
        true ->
            case is_empty_stack(Stack) of
                true -> true;
                false -> false
            end;
        false ->
            {New_Input_Queue, DeQueue_Value} = dequeue(Input_Queue),
            case DeQueue_Value /= peek_queue(Output_Queue) of
                true ->
                    NewStack = push_stack(Stack, DeQueue_Value),
                    perm_helper_1(New_Input_Queue, Output_Queue, NewStack);
                false ->
                    {New_Output_Queue, _} = dequeue(Output_Queue),
                    {Final_Output_Queue, NewStack} = perm_helper_2(New_Output_Queue, Stack),
                    perm_helper_1(New_Input_Queue, Final_Output_Queue, NewStack)
            end
    end.



-spec perm_helper_2(queue:queue(T), queue:queue(T)) -> {queue:queue(T), queue:queue(T)}.
perm_helper_2(Output_Queue, Stack) ->
    case is_empty_stack(Stack) of 
        true ->
            {Output_Queue, Stack};
        false ->
            Peek_Queue_Value = peek_queue(Output_Queue),
            Peek_Stack_Value = peek_stack(Stack),
            case Peek_Queue_Value =:= Peek_Stack_Value of
                true ->
                    {New_Output_Queue, _} = dequeue(Output_Queue),
                    {NewStack, _} = pop_stack(Stack),
                    perm_helper_2(New_Output_Queue, NewStack);
                false ->
                    {Output_Queue, Stack}
            end
    end.


-spec sort_test() -> ok.
sort_test() ->
    true = perm([], []),
    true = perm([1], [1]),
    false = perm([1], [2]),
    true = perm([1, 2, 3], [3, 1, 2]),
    false = perm([1, 2, 3], [2, 3, 1]),
    ok.