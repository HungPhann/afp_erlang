-module(perm).
-export([perm/2]).

-include_lib("proper/include/proper.hrl").
-include_lib("eunit/include/eunit.hrl").

% helper functions

% stack
-spec new_stack() -> queue:queue().
new_stack() ->
    queue:new().

-spec push_stack(queue:queue(T), T) -> queue:queue(T).
push_stack(Stack, Value) ->
    queue:in_r(Value, Stack).

-spec pop_stack(queue:queue(T)) -> {queue:queue(T), T}.
pop_stack(Stack) ->
    {{_, Value}, NewStack} = queue:out(Stack),
    {NewStack, Value}.

-spec peek_stack(queue:queue(T)) -> T.
peek_stack(Stack)->
    {_, Value} = queue:peek(Stack),
    Value.

-spec is_empty_stack(queue:queue()) -> boolean.
is_empty_stack(Stack) ->
    queue:is_empty(Stack).


% queue
-spec list_to_queue([T]) -> queue:queue(T).
list_to_queue(List) -> queue:from_list(List).

-spec dequeue(queue:queue(T)) -> {queue:queue(T), T}.
dequeue(Queue) ->
    {{_, Value}, NewQueue} = queue:out_r(Queue),
    {NewQueue, Value}.

-spec peek_queue(queue:queue(T)) -> T.
peek_queue(Queue) ->
    {_, Value} = queue:peek_r(Queue),
    Value.

-spec is_empty_queue(queue:queue()) -> boolean.
is_empty_queue(Queue) ->
    queue:is_empty(Queue).


% main logic

-spec perm([T], [T]) -> boolean.
perm(Input_List, Output_List) ->
    Input_Queue = list_to_queue(Input_List),
    Output_Queue = list_to_queue(Output_List),
    Stack = new_stack(),
    perm_helper_1(Input_Queue, Output_Queue, Stack).
    

-spec perm_helper_1(queue:queue(T), queue:queue(T), queue:queue(T)) -> boolean.
perm_helper_1(Input_Queue, Output_Queue, Stack) ->
    case is_empty_queue(Input_Queue) of
        true ->
            case is_empty_stack(Stack) of
                true -> true;
                false -> false
            end;
        false ->
            {New_Input_Queue, DeQueue_Value} = dequeue(Input_Queue),
            case DeQueue_Value /= peek_queue(Output_Queue) of
                true ->
                    NewStack = push_stack(Stack, DeQueue_Value),
                    perm_helper_1(New_Input_Queue, Output_Queue, NewStack);
                false ->
                    {New_Output_Queue, _} = dequeue(Output_Queue),
                    {Final_Output_Queue, NewStack} = perm_helper_2(New_Output_Queue, Stack),
                    perm_helper_1(New_Input_Queue, Final_Output_Queue, NewStack)
            end
    end.



-spec perm_helper_2(queue:queue(T), queue:queue(T)) -> {queue:queue(T), queue:queue(T)}.
perm_helper_2(Output_Queue, Stack) ->
    case is_empty_stack(Stack) of 
        true ->
            {Output_Queue, Stack};
        false ->
            Peek_Queue_Value = peek_queue(Output_Queue),
            Peek_Stack_Value = peek_stack(Stack),
            case Peek_Queue_Value =:= Peek_Stack_Value of
                true ->
                    {New_Output_Queue, _} = dequeue(Output_Queue),
                    {NewStack, _} = pop_stack(Stack),
                    perm_helper_2(New_Output_Queue, NewStack);
                false ->
                    {Output_Queue, Stack}
            end
    end.



% test 

% manual test
perm_test_() ->
    [test_zero(), test_one(), test_three(), test_eight()].

test_zero() ->
    [?_assertEqual(true, perm([], []))].

test_one() -> 
    [?_assertEqual(true, perm([1], [1])),
    ?_assertEqual(false, perm([1], [2]))].

test_three() ->
    [?_assertEqual(true, perm([1, 2, 3], [3, 1, 2])),
    ?_assertEqual(false, perm([1, 2, 3], [2, 3, 1]))].

test_eight() ->
    [?_assertEqual(true, perm([1, 2, 3, 4, 5, 6, 7, 8], [3, 1, 2, 8, 6, 4, 5, 7])),
    ?_assertEqual(false, perm([3, 1, 2, 8, 6, 4, 5, 7], [1, 2, 3, 4, 5, 6, 7, 8]))].



% property-based test
prop_perm() ->
    ?FORALL({L1, L2}, list_no_dupls(integer()),
            perm(L1, L2) =:= perm (lists:reverse(L2), lists:reverse(L1))).


% generate valid lists for prop_perm test.
list_no_dupls(T) ->
    ?LET({L1, L2}, {list(T), list(T)}, perm_get_test_lists(L1, L2)).


% provided 2 list L1, L2, the result is tuple {R1, R2} where:
%   R1 is sublist of L1 without duplication
%   R2 is sublist of L1 without duplication
%   length(R1) = length(R2)
-spec perm_get_test_lists([T], [T]) -> {[T], [T]}.
perm_get_test_lists(L1, L2) ->
    L1_No_Duplicate = remove_duplicates(L1),
    L2_No_Duplicate = remove_duplicates(L2),
    case length(L1_No_Duplicate) > length(L2_No_Duplicate) of 
        true ->
            {lists:sublist(L1_No_Duplicate, length(L2_No_Duplicate)), L2_No_Duplicate};
        false ->
            {L1_No_Duplicate, lists:sublist(L2_No_Duplicate, length(L1_No_Duplicate))}
    end.


% remove duplicate from list.
-spec remove_duplicates([_]) -> [_].
remove_duplicates([]) -> [];
remove_duplicates([A|T]) ->
    case lists:member(A, T) of
        true -> remove_duplicates(T);
        false -> [A|remove_duplicates(T)]
    end.
