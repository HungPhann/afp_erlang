#!/usr/bin/env python3
import sys
import string
import random
n = int(sys.argv[1])

for i in range(1, n):
    print(i, end=' ')
print(n)

for i in range(n, 1, -1):
    print(i, end=' ')
print(1)

